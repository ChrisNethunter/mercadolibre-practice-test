import { createReducer } from '@reduxjs/toolkit';
import { ResponQuery } from '../../models/interfaces/product.interface';
import {
  fetchCategoryProducts,
  fetchDescriptionProduct,
  fetchDetailProduct,
  fetchProductsQuery,
} from './products.actions';

interface ProductsState {
  query: ResponQuery | {};
  detailProduct: any | {};
  descriptionProduct: any | {};
  categoryProduct: any | {};
}

const initialState: ProductsState = {
  query: {
    products: [],
    filters: [],
    available_filters: [],
    categories: [],
  },
  detailProduct: {},
  descriptionProduct: {},
  categoryProduct: {},
};

export const productsReducer = createReducer(initialState, (builder) => {
  builder.addCase(fetchProductsQuery.pending, (state) => ({
    ...state,
    query: {},
  }));

  builder.addCase(fetchProductsQuery.rejected, (state) => ({
    ...state,
    query: {},
  }));

  builder.addCase(fetchProductsQuery.fulfilled, (state, action) => ({
    ...state,
    query: action.payload,
  }));

  builder.addCase(fetchDetailProduct.pending, (state) => ({
    ...state,
    detailProduct: {},
  }));

  builder.addCase(fetchDetailProduct.rejected, (state) => ({
    ...state,
    detailProduct: {},
  }));

  builder.addCase(fetchDetailProduct.fulfilled, (state, action) => ({
    ...state,
    detailProduct: action.payload,
  }));

  builder.addCase(fetchDescriptionProduct.pending, (state) => ({
    ...state,
    descriptionProduct: {},
  }));

  builder.addCase(fetchDescriptionProduct.rejected, (state) => ({
    ...state,
    descriptionProduct: {},
  }));

  builder.addCase(fetchDescriptionProduct.fulfilled, (state, action) => ({
    ...state,
    descriptionProduct: action.payload,
  }));

  builder.addCase(fetchCategoryProducts.pending, (state) => ({
    ...state,
    categoryProduct: {},
  }));

  builder.addCase(fetchCategoryProducts.rejected, (state) => ({
    ...state,
    categoryProduct: {},
  }));

  builder.addCase(fetchCategoryProducts.fulfilled, (state, action) => ({
    ...state,
    categoryProduct: action.payload,
  }));
});
