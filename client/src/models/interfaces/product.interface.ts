export interface ResponQuery {
  products: Products[] | [];
  filters: any;
  available_filters: any;
  categories: any;
}

export interface Products {
  id: string;
  title: string;
  address: any;
  thumbnail: string;
  price: number;
}

export interface Address {
  city_id: string;
  city_name: string;
  state_id: string;
  state_name: string;
}

export type Query = {
  query: string;
};

export type QueryProduct = {
  id: string;
};
