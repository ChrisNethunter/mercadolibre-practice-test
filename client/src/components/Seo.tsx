import { Helmet } from 'react-helmet';
import ico from '../images/logo.png';

interface PropsSeo {
  title: string;
  description?: string;
  url_image?: string;
}

export default function Seo({ title, description, url_image }: PropsSeo) {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{title}</title>
      <link rel="icon" href={ico} type="image/x-icon" />
      <link rel="canonical" href="https://www.mercadolibre.com.co/" />
      <meta
        name="description"
        content={description ? description : 'Clone mercadolibre test'}
        key="desc"
      />

      <meta property="og:type" content="metadata" />
      <meta property="og:title" content={title} />
      <meta
        property="og:description"
        content={description ? description : 'Clone mercadolibre test'}
      />
      <meta
        property="og:image"
        content={
          url_image ? url_image : 'https://example.com/images/cool-page.jpg'
        }
      />
      <meta
        property="keywords"
        content="productos,compra,vende,tecnologia..."
      />
      <meta property="og:url" content="https://www.mercadolibre.com.co/" />
      <meta property="og:site_name" content="mercadolibre.com" />
      <meta
        property="og:image"
        content={
          url_image ? url_image : 'https://example.com/images/cool-page.jpg'
        }
      />

      <meta name="twitter:card" />
      <meta name="twitter:site" content="@mercadolibre" />
      <meta name="twitter:title" content={title} />
      <meta
        name="twitter:image"
        content={
          url_image ? url_image : 'https://example.com/images/cool-page.jpg'
        }
      />
      <meta
        name="twitter:description"
        content={description ? description : 'Clone mercadolibre test'}
      />
      <meta name="twitter:creator" content="@mercadolibre" />
    </Helmet>
  );
}
