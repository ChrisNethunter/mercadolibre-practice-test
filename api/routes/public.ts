import { Request, Response } from "express";
import request_search from "../helpers/request";
import ResponseApi from "../helpers/response";

import config from "../config/config.json";

const CURRENT_ENV = process.env.ENVIRONMENT || "development";
const ur_api = config[CURRENT_ENV].URL_ML;

class Public {
  url_api: string;

  constructor() {
    this.url_api = config[CURRENT_ENV].URL_ML;
  }

  async search_product(req: Request, res: Response) {
    let query = req.body.query;
    if (!query) {
      return res.status(200).json({
        success: false,
      });
    }

    let url = `${this.url_api}/sites/MLA/search?q=${query}`,
      ml_api_products: any = await request_search(url);

    if (!ml_api_products.success) {
      return res.status(200).json({
        success: false,
      });
    }

    let categories = [];
    let find_key_categories = ml_api_products.data.available_filters.findIndex(
      (item) => {
        return item.id == "category";
      }
    );

    if (find_key_categories > -1) {
      categories = ml_api_products.data.available_filters[find_key_categories];
      let items: any = categories.values;
      items.sort(function (a, b) {
        return b.results - a.results;
      });
      categories = items;
      categories.length = 4;
    }

    let data = {
      success: ml_api_products.success ? ml_api_products.success : false,
      data: {
        products: ml_api_products.data.results,
        filters: ml_api_products.data.filters,
        available_filters: ml_api_products.data.available_filters,
        categories,
      },
    };
    return await ResponseApi(req, res, data);
  }

  async detail_product(req: Request, res: Response) {
    let query = req.body.id;
    if (!query) {
      return res.status(200).json({
        success: false,
      });
    }
    let url = `${this.url_api}/items/` + query;
    let ml_api_products: any = await request_search(url.trim());
    let response = {
      success: ml_api_products.success ? ml_api_products.success : false,
      data: ml_api_products.data,
    };

    if (ml_api_products.success) {
      let category = await this.data_category(ml_api_products.data.category_id);
      response.data.category = category;
    }

    return ResponseApi(req, res, response);
  }

  async description_product(req: Request, res: Response) {
    let query = req.body.id;
    if (!query) {
      return res.status(200).json({
        success: false,
      });
    }
    let url = `${this.url_api}/items/${query}/description`;
    let ml_api_products: any = await request_search(url.trim());
    let data = {
      success: ml_api_products.success ? ml_api_products.success : false,
      data: ml_api_products.data,
    };
    return ResponseApi(req, res, data);
  }

  async data_category(category_id) {
    let query = category_id;
    let url = `${this.url_api}/categories/${query}`,
      ml_api_products: any = await request_search(url);
    if (ml_api_products.success) {
      return ml_api_products.data;
    }
    return [];
  }
}

module.exports = Public;
