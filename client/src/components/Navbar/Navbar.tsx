import React, { useEffect, useState } from 'react';

import search from '../../images/icon_search.png';
import logo from '../../images/logo.png';

import Utils from '../../helpers/utils';

import './navbar.sass';

export default function Navbar() {
  const [query, setQuery] = useState<string>('');

  useEffect(() => {
    let query = Utils.getParamsUrl('search');
    if (query) {
      setQuery(query);
    }
  }, []);

  function handlerSubmit(event: React.SyntheticEvent) {
    event.preventDefault();
    if (query != '') {
      window.location.href = `${window.location.origin}/items?search=${query}`;
    }
  }

  return (
    <div>
      <nav className="navbar-ml navbar-expand-ml ">
        <div className="container nav-margin">
          <a className="navbar-brand" href="/">
            <img
              src={logo}
              alt="logo_mercadolibre"
              width={56}
              height={36}
              className="d-inline-block align-text-top"
            />
          </a>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <form className="d-flex search-ml" onSubmit={handlerSubmit}>
              <input
                id="search-ml"
                onChange={(event) => {
                  setQuery(event.currentTarget.value);
                }}
                value={query}
                className="form-ml"
                type="text"
                placeholder="Nunca dejes de buscar"
                aria-label="Search"
              />

              <button className="btn-ml btn-light-ml" type="submit">
                <img src={search} alt="buscar" width={17} height={17} />
              </button>
            </form>
          </div>
        </div>
      </nav>
    </div>
  );
}
