import bodyParser from "body-parser";
import cors from "cors";
import express, { Express } from "express";
import fs from "fs";
import helmet from "helmet";
import http from "http";
import path from "path";
import { authorMiddleware, validateApiMiddleware } from "./middleware";
import { ConfigTypes } from "./types/config";

declare global {
  var config: ConfigTypes;
}
global.config = require("./config/config.json");

class Server {
  app: Express;
  port: number | string;
  server: any;
  enviroment: string;

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 3001;
    this.server = http.createServer(this.app);
    this.enviroment = process.env.NODE_ENV || "development";

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.app.use(
      "*",
      cors({
        origin: function (origin: any, callback: any) {
          callback(null, true);
        },
      })
    );
    this.app.disable("x-powered-by");
    this.app.use(helmet({ contentSecurityPolicy: false }));
    this.app.set("trust proxy", 1);
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(express.static(path.join(__dirname, "build")));
  }

  routes() {
    var routes = {};
    fs.readdirSync(__dirname + "/routes").forEach(function (file) {
      var moduleName = file.split(".")[0];
      routes[moduleName] = require(__dirname + "/routes/" + moduleName);
    });
    this.app.use(authorMiddleware);
    this.app.post("/api/:route/:method", validateApiMiddleware);
    this.app.get("/api/:route/:method", validateApiMiddleware);
  }

  async execute() {
    return this.listen();
  }

  listen() {
    this.server.listen(this.port, () => {
      console.log(
        `⚡️[server]: Server ${this.enviroment} is running at http://localhost:${this.port}`
      );
    });
  }
}

export default Server;
