interface LayoutProps {
  children: React.ReactNode;
}
export default function Container({ children }: LayoutProps) {
  return <div className="container">{children}</div>;
}
