import { API_URL } from '../../constants/env';
import { Query, QueryProduct } from '../../models/interfaces/product.interface';

const queryProduct = async (data: Query): Promise<any> => {
  return await Request(data, '/api/public/search_product');
};

const fetchDetailProduct = async (data: QueryProduct): Promise<any> => {
  return await Request(data, '/api/public/detail_product');
};

const fetchDescriptionProduct = async (data: QueryProduct): Promise<any> => {
  return await Request(data, '/api/public/description_product');
};

const fetchProductCategory = async (data: QueryProduct): Promise<any> => {
  return await Request(data, '/api/public/data_category');
};

export const ProductService = {
  queryProduct,
  fetchDetailProduct,
  fetchDescriptionProduct,
  fetchProductCategory,
};

function Request(data: any, route: string) {
  return new Promise((resolve) => {
    fetch(`${API_URL}${route}`, {
      body: JSON.stringify(data),
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
    }).then(async (response) => {
      response = await response.json();
      if ('data' in response) {
        return resolve(response.data);
      }
    });
  });
}
