import { Products } from '../../models/interfaces/product.interface';
import './itemProduct.sass';

import icon_ship from '../../images/icon_ship.png';
const image_standar = 'http://via.placeholder.com/180x180';

export default function ItemProduct({
  thumbnail,
  address,
  title,
  price,
  id,
}: Products) {
  const handlerClickDetail = (event: React.SyntheticEvent) => {
    event.preventDefault();
    window.location.replace('/item/' + event.currentTarget.id);
  };

  return (
    <div id={id} className="card item-result-ml" onClick={handlerClickDetail}>
      <div className="row ">
        <div className="col-image-result">
          <img
            src={thumbnail ? thumbnail : image_standar}
            className="search-result-products-ml img-fluid rounded-start"
            alt={title}
          />
        </div>
        <div className="col-description-result container-description">
          <div className="card-body">
            <h2 className="card-title ml-item-title">
              $ {price ? Number(price).toLocaleString() : 0}{' '}
              <img src={icon_ship} className="ml-image-shipping" />
            </h2>
            <p className="card-text ml-item-description"> {title} </p>
          </div>
        </div>

        <div className="container-description">
          <div className="card-body">
            <p className="align-middle">{address}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
