import { Request, Response } from "express";

export default function validateApiMiddleware(req: Request, res: Response) {
  try {
    let route = new (require("../routes/" + req.params.route + ".ts"))();
    return route[req.params.method](req, res);
  } catch (e) {
    console.log(
      "API HAS CRASHED - " + e.message,
      req.params.route + "/" + req.params.method
    );
    res.status(500).json({
      success: false,
      data: "Internal error endpoint",
    });
  }
}
