import { Request, Response } from "express";

export default function ResponseApi(req: Request, res: Response, data) {
  const author = res.author;
  data.author = author;
  return res.status(200).json({ ...data });
}
