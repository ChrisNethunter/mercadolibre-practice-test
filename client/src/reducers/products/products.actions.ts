import { createAction, createAsyncThunk } from '@reduxjs/toolkit';

import { ProductService } from './products.service';

export const fetchProductsQuery = createAsyncThunk(
  'products/fetchProductsData',
  ProductService.queryProduct,
);

export const fetchDetailProduct = createAsyncThunk(
  'products/fetchDetailProduct',
  ProductService.fetchDetailProduct,
);

export const fetchDescriptionProduct = createAsyncThunk(
  'products/fetchDescriptionProduct',
  ProductService.fetchDescriptionProduct,
);

export const fetchCategoryProducts = createAsyncThunk(
  'products/fetchCategoryProduct',
  ProductService.fetchProductCategory,
);

export const setProductResults = createAction<any | null>(
  'products/setProductResults',
);
