interface LayoutProps {
  children: React.ReactNode;
}
export default function ContainersResults({ children }: LayoutProps) {
  return <div className="content-results-ml">{children}</div>;
}
