import { RootState } from '../../app/store';

export const selectProductsQuery = (state: RootState) => state.products.query;

export const selectProductDetail = (state: RootState) =>
  state.products.detailProduct;

export const selectProductDescription = (state: RootState) =>
  state.products.descriptionProduct;

export const selectProductCategory = (state: RootState) =>
  state.products.categoryProduct;
