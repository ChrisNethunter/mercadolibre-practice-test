declare global {
  namespace Express {
    interface Response {
      author: string;
    }
  }
}
export default function authorMiddleware(req: any, res: any, next: any) {
  res.author = "Christian Jimenez";
  next();
}
