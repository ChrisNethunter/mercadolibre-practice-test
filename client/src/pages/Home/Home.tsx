import Container from '../../components/Containers/Container';
import Seo from '../../components/Seo';

export default function Home() {
  return (
    <Container>
      <Seo title="Mercadolibre clone" />
    </Container>
  );
}
