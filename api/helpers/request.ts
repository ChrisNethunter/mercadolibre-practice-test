export default function request_search(url: string) {
  return new Promise(async (resolve, reject) => {
    const superagent = require("superagent");

    superagent.get(url).end((err, res) => {
      if (!err) {
        return resolve({
          success: true,
          data: res.body,
        });
      } else {
        return resolve({
          success: false,
          data: err,
        });
      }
    });
  });
}
