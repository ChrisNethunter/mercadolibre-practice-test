import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import MainLayout from './layouts/MainLayout';
import DetailProduct from './pages/DetailProduct/DetailProduct';
import Home from './pages/Home/Home';
import SearchResults from './pages/SearchResults/SearchResults';

const App = () => (
  <Router>
    <MainLayout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/items" element={<SearchResults />} />
        <Route path="/item/:id" element={<DetailProduct />} />
      </Routes>
    </MainLayout>
  </Router>
);

export default App;
