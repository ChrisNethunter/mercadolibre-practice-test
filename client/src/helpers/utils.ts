export default new (class Utils {
  getParamsUrl(param: string): string {
    let url = new URL(window.location.href);
    let query = url.searchParams.get(param);
    return query || '';
  }
})();
