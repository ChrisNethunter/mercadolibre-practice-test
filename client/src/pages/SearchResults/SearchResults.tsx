import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../../app/hooks';

import { fetchProductsQuery } from '../../reducers/products/products.actions';
import { selectProductsQuery } from '../../reducers/products/products.selectors';

import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import Container from '../../components/Containers/Container';
import ContainersResults from '../../components/Containers/ContainersResults';
import ItemProduct from '../../components/ItemProduct/ItemProduct';
import Seo from '../../components/Seo';

import Utils from '../../helpers/utils';

import './results.sass';

export default function SearchResults() {
  const dispatch = useAppDispatch();
  const queryFetch = useSelector(selectProductsQuery);
  const [query, setQuery] = useState<string>('');
  useEffect(() => {
    fetchQuery();
  }, []);

  const fetchQuery = () => {
    let query = Utils.getParamsUrl('search');
    if (query) {
      setQuery(query);
      let data = {
        query,
      };
      dispatch(fetchProductsQuery(data));
    }
  };

  const validateDataBreadcrum = () => {
    if ('categories' in queryFetch && queryFetch.categories.length > 0) {
      return <Breadcrumb filters={queryFetch.categories} />;
    }

    if ('filters' in queryFetch && queryFetch.filters.length > 0) {
      return (
        <Breadcrumb filters={queryFetch.filters[0].values[0].path_from_root} />
      );
    }

    return '';
  };

  return (
    <Container>
      <Seo title={`${query} | Mercadolibre`} />

      {validateDataBreadcrum()}

      <ContainersResults>
        {'products' in queryFetch && queryFetch.products.length > 0
          ? queryFetch.products.map((product: any, key: number) => (
              <ItemProduct
                key={key}
                id={product.id}
                thumbnail={product.thumbnail}
                price={product.price}
                title={product.title}
                address={product.address.state_name}
              />
            ))
          : ''}
      </ContainersResults>
    </Container>
  );
}
