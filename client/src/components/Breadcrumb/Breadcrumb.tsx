import { useEffect, useState } from 'react';

import './breadcrumb.sass';
interface Filter {
  id: string;
  name: string;
  text: string;
  values: any;
}

interface FiltersProps {
  filters: any;
}

export default function Breadcrumb({ filters }: FiltersProps) {
  const [filterData, setFilterData] = useState([]);
  useEffect(() => {}, []);

  return (
    <nav>
      <ol className="breadcrumb-ml">
        {filters.map((filter: Filter, key: number) => (
          <li id={filter.id} key={key} className="breadcrumb-item-ml">
            {filter.name}
          </li>
        ))}
      </ol>
    </nav>
  );
}
