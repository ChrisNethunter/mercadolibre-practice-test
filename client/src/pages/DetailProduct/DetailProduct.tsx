import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useAppDispatch } from '../../app/hooks';

import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
import Seo from '../../components/Seo';

import {
  fetchDescriptionProduct,
  fetchDetailProduct,
} from '../../reducers/products/products.actions';
import {
  selectProductDescription,
  selectProductDetail,
} from '../../reducers/products/products.selectors';

import Container from '../../components/Containers/Container';
import ContainersResults from '../../components/Containers/ContainersResults';

import './detailProduct.sass';

export default function DetailProduct() {
  const dispatch = useAppDispatch();
  const dataFetchProduct = useSelector(selectProductDetail);
  const dataFetchDescription = useSelector(selectProductDescription);
  //const dataFetchCategory = useSelector(selectProductCategory);

  let { id } = useParams();

  useEffect(() => {
    fetchDetail();
  }, []);

  const fetchDetail = async () => {
    if (id) {
      let data = {
        id,
      };
      await dispatch(fetchDetailProduct(data));
      await dispatch(fetchDescriptionProduct(data));
    }
  };

  if (Object.keys(dataFetchProduct).length == 0) {
    return (
      <Container>
        <h2 className="loading">Cargando datos de producto</h2>
      </Container>
    );
  }

  return (
    <Container>
      <Seo
        title={dataFetchProduct.title}
        description={dataFetchDescription.plain_text}
        url_image={dataFetchProduct.pictures[0].secure_url}
      />

      <Breadcrumb filters={dataFetchProduct.category.path_from_root} />

      <ContainersResults>
        <div className="row" style={{ background: 'white' }}>
          <div className="col-detail-product">
            <img
              alt={dataFetchProduct.title}
              src={dataFetchProduct.pictures[0].secure_url}
            />
          </div>
          <div className="col-detail-price">
            <div className="container-ml-detail-product">
              <p className="quantity-ml-text margin-ml-detail">
                {dataFetchProduct.condition == 'new'
                  ? 'Nuevo'
                  : dataFetchProduct.condition}{' '}
                -{' '}
                {Number(dataFetchProduct.initial_quantity) -
                  Number(dataFetchProduct.available_quantity)}{' '}
                vendidos
              </p>

              <h3 className="title-ml-product">
                <b>{dataFetchProduct.title}</b>
              </h3>

              <h3 className="price-ml-product margin-ml-detail">
                $ {Number(dataFetchProduct.price).toLocaleString()}
              </h3>

              <div className="margin-ml-detail">
                <button className="btn-ml btn-primary-ml btn-block btn-ml">
                  Comprar
                </button>
              </div>
            </div>
          </div>

          <div className="container-ml-description-prod col-md-6  margin-ml-detail-left">
            <h2 className="title-ml-description-prod">
              Descripción de producto{' '}
            </h2>
            <div
              className="description-ml margin-ml-detail"
              style={{ width: '69%' }}>
              <p>{dataFetchDescription.plain_text}</p>
            </div>
          </div>
        </div>
      </ContainersResults>
    </Container>
  );
}
